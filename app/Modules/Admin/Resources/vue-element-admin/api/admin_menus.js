import request from '@/utils/request'

export function getMenusSelect() {
    return request({
        url: '/admin_menus/getSelectLists',
        method: 'get'
    })
}

export function getList(params, get_url = false) {
    var url = '/admin_menus';
    if (get_url) return url;
    return request({
        url: url,
        method: 'get',
        params
    })
}

export function create(data, get_url = false) {
    var url = `/admin_menus/create`;
    if (get_url) return url;
    return request({
        url: url,
        method: 'post',
        data
    })
}

export function update(data, get_url = false) {
    var url = `/admin_menus/update`;
    if (get_url) return url;
    return request({
        url: url,
        method: 'put',
        data
    })
}

export function setDel(data, get_url = false) {
    var url = `/admin_menus/delete`;
    if (get_url) return url;
    return request({
        url: url,
        method: 'delete',
        data
    })
}

export function changeFiledStatus(data, get_url = false) {
    var url = `/admin_menus/changeFiledStatus`;
    if (get_url) return url;
    return request({
        url: url,
        method: 'put',
        data
    })
}
