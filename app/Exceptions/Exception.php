<?php

namespace App\Exceptions;

use App\Traits\Json;
use Throwable;

class Exception extends \Exception
{
    use Json;

    protected $msg;

    public function __construct($message = "success", $http_status = 400, Throwable $previous = null)
    {
        parent::__construct($message, $http_status, $previous);

        $this->msg = $message;
        $this->setHttpCode($http_status);
    }
}
