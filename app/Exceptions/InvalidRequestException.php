<?php

namespace App\Exceptions;

use Illuminate\Http\Request;

class InvalidRequestException extends Exception
{
    public function __construct(string $message = "", int $http_status = 400)
    {
        parent::__construct($message, $http_status);
    }

    public function render(Request $request)
    {
        if ($request->expectsJson()) {
            return $this->errorJson($this->getMessage(), $this->getCode());
        }
    }
}
